# PaPh0


Pa[ra]Ph0[ny] is a little arduino based project wich split two midi note messages into a midi note message and two midi CC messages for oscillators tunning.


## But... Why?

Well, simply because monosynth can't handle two notes (You know, MONO) but if it has two oscillators one can tune each of them to the right frequency to achieve paraphony (but it's kind of laborious so an Arduino is fine).

## NB

I only used this project with a DSI MoPho, if you want it to work with another synth you must change midi CC numbers in the source code.
```
int osc1=20;
int osc2=24;
```