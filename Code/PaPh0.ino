#include <MIDI.h>
byte noteplayed [2];
bool isnoteplayed [] = {false, false};
unsigned long int noteplayedtime[2];
int noteplayedvel;
int notereceive;
bool noteon = false;
byte data;
byte notebyte;
byte velocitybyte;
int midiInChannel = 1;
int midiOutChannel = 3;
unsigned long int temptime = 0;
bool ok = false;
bool alloff = true;

int osc1=20;
int osc2=24;

MIDI_CREATE_DEFAULT_INSTANCE();
void PreNoteOff(byte notebyte)
{
  for (int i = 0; i < 2; i++)
  {
    if ((int)notebyte == noteplayed[i])
    {
      isnoteplayed[i] = false;
      if (isnoteplayed[0] == false and isnoteplayed[1] == false)
      {
        MIDI.sendNoteOff(0, noteplayedvel, midiOutChannel);
      }
      else{
      if (i == 0)
        MIDI.sendControlChange (osc1, noteplayed[1], midiOutChannel);
      if (i == 1)
        MIDI.sendControlChange (osc2, noteplayed[0], midiOutChannel);
      }
    }
  }
}
void noteOn(byte channel, byte note, byte velocity)    //please refer to midi standard reference
{
  temptime = micros();
  if (isnoteplayed[0] == false and isnoteplayed[1] == false)
    alloff = true;
  else alloff = false;
  if ((((noteplayedtime[0] - temptime) < (noteplayedtime[1] - temptime)) and isnoteplayed[0] == true) or isnoteplayed[1] == false)
  {
    noteplayed[1] = note;
    noteplayedtime[1] = micros();
    noteplayedvel = velocity;
    MIDI.sendControlChange (osc2, noteplayed[1], midiOutChannel);
    isnoteplayed[1] = true;
    if (alloff)
      MIDI.sendControlChange (osc1, noteplayed[1], midiOutChannel);
  }
  else
  {
    noteplayed[0] = note;
    noteplayedtime[0] = micros();
    noteplayedvel = velocity;
    MIDI.sendControlChange (osc1, noteplayed[0], midiOutChannel);
    isnoteplayed[0] = true;
    if (alloff)
      MIDI.sendControlChange (osc2, noteplayed[1], midiOutChannel);
  }
  if (alloff)
    MIDI.sendNoteOn(0, noteplayedvel, midiOutChannel);
}
void noteOff(byte channel, byte note, byte velocity)
{
  PreNoteOff(note);
}
void setup() {
  Serial.begin(31250);
  MIDI.setHandleNoteOn(noteOn);
  MIDI.setHandleNoteOff(noteOff);
  MIDI.begin(midiInChannel);
}
void loop() {
  MIDI.read();
}
